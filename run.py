#!/home/asad/anaconda/envs/voice2txt/bin/python

import numpy as np
import sys
from utils import extract_audio
from utils import extract_txt
from utils import translate
from utils import txt2Speech
from utils import googleText2Speech

def main():
    if len(sys.argv)<3:
        raise "Specify the video file and target language \n example: run.py ex.mp4 hi"
    else:
        print(f"Processing the video file ",sys.argv[1])
        extract_audio(sys.argv[1])
        print(sys.argv[1].split('.')[0])
        audio_file=sys.argv[1].split('.')[0]+".wav"
        lang=sys.argv[2]
        txt_file=extract_txt(audio_file) 
       #txt_file="/home/asad/voice2txt/data/sample4_google_.txt"
        translated_txt=translate(txt_file,lang)
        #txt2Speech(translated_txt)
        googleText2Speech(translated_txt,lang)
        
if __name__=="__main__":
    main()
