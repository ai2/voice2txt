import numpy as np
import subprocess
import tqdm
import speech_recognition as sr
import os
import googletrans
from google_speech import Speech
from gtts import gTTS

def extract_audio(in_video):
    """Extract audio from video using ffmpeg"""    
    if(os.path.exists(in_video.split('.')[0]+".wav")):
        print("Audio File Exists")
        return

    print(f"Extracting the Audio from video ",in_video)
    try:
        # single channel 16k audio extraction.Important for methods like sphenix
        command = "ffmpeg -i " +in_video+" -acodec pcm_s16le -ac 1 -ar 16000 -b:a 706k "+in_video.split('.')[0]+".wav | tdqm"
        subprocess.call(command,shell=True)
    except :
        print("Cannot extract the audio file")
        raise "Aborting!"
    
    print("Audio Extraction Successfull!")


def extract_txt(audio_file,method="google"):
    """Extract transcription from audiofile"""
    print (f"Using speech recgniton version {sr.__version__}")
    r=sr.Recognizer()
    fileprefix=audio_file.split('.')[0]
    with sr.AudioFile(audio_file) as source:
        audio=r.record(source)
    print("Audio extracted for transcription")
    if method=="google":
        try:
            filename=fileprefix+"_google_"+".txt"
            txt=r.recognize_google(audio)
            with open(filename,"w") as f:
                f.write(txt)
            print(f"Text extracted is \n {txt}")
            return filename
        except sr.RequestError as e:
            print(f"Cannot get text from audio {e}")
        except sr.UnknownValueError as e:
            print("Unable to understand the audio")
    elif method=="sphinx":
        try:
            #print(audio)
            sphinx_txt=r.recognize_sphinx(audio)
            filename=fileprefix+"__sphinx__"+".txt"
            print(f"Sphinx Extracted text is \n {sphinx_txt}")
            with open(filename,"w") as f:
                f.write(sphinx_txt)
            return filename
        except sr.UnknownValueError:
            print("Sphinix didnot understand the audio")
        except sr.RequestError as e:
            print(f"Cannot coonect {e}")


def translate(filename,target='ur',method="google"):
    """Translate the text to some target language"""
    with open(filename,"r") as f:
        txt=f.readline()
        print(txt)
    if method=="google":
        translator=googletrans.Translator()
        dt=translator.detect(txt)
        #print(dir(dt))
        print(f"Language detected {dt.lang}")
        translation=translator.translate(txt,src=dt.lang,dest=target)
        print(dir(translation))
        print(f"Translated txt is \n {translation.text}")
        with open("data/translated"+target+".txt","w") as f:
            f.write(translation.text)
        return translation.text

def txt2Speech(txt,target="ur"):
    speech=Speech(txt,target)
    print("Playing the translated speech")
    speech.play()
    speech.save("data/target.wav") 

def googleText2Speech(in_txt,target="ur"):
    tts=gTTS(in_txt,lang=target,slow=False)
    tts.save("data/translation"+target+".wav")
