# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Repo for language translation and speechsynthesis  ###


### How to run?###

"python3 run.py inputVideo.mp4 tagetlanguage"


Example for Hindi: #python3 run.py data/sample4.mp4 hi# 


Example for Geraman: #python3 run.py data/sample4.mp4 de#


### Output ####
Output is wav file and translated text
Can be fed to LipGAn or other methods for synthesis

### Progress ###

* Speech synthesis using Tacotron2

### TO DO ###

* Better text extraction part (currently tested google and Sphinx ) 
* Better Translation part 


